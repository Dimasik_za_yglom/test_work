package com.company;

public class Circle extends Ellipse implements Shift {

    public Circle(double x) {
        super(x, x);
    }

    @Override
    double getPerimeter() {
        p = 2 * x * Math.PI;
        return p;
    }

    @Override
    public void shiftY(double y) {
        centreY = y;
    }

    @Override
    public void shiftX(double x) {
        centreX = x;
    }
}
