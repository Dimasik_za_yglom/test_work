package com.company;

public class Square extends Rectangle implements Shift {

    public Square(double x) {
        super(x, x);
    }

    @Override
    public void shiftY(double y) {
        centreY = y;
    }

    @Override
    public void shiftX(double x) {
        centreX = x;
    }

    // периметр Rectangle подходит и для квадрата
}
