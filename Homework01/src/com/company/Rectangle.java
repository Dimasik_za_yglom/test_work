package com.company;

public class Rectangle extends Figure {

    public Rectangle(double x, double y) {
        super(x, y);
    }

    @Override
    double getPerimeter() {
        p = (x + y) * 2;
        return p;
    }
}
