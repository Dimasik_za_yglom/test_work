package com.company;

public abstract class Figure {
    protected double x;
    protected double y;
    protected double p;
    protected double centreX;
    protected double centreY;


    public Figure(double x, double y) {
        this.x = x;
        this.y = y;
    }

    abstract double getPerimeter();

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public void setX(double x) {
        this.x = x;
    }

    public void setY(double y) {
        this.y = y;
    }

    public double getCentreX() {
        return centreX;
    }

    public double getCentreY() {
        return centreY;
    }

    void print(double value) {
        System.out.println("Периметр фигуры: " + value + " см.");
    }

}
