package com.company;

public class Ellipse extends Figure {

    public Ellipse(double x, double y) {
        super(x, y);
    }

    @Override
    double getPerimeter() {
        p = 4 * ((Math.PI * x * y + Math.pow((x - y), 2.0)) / (x + y));
        return p;
    }
}
