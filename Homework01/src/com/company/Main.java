package com.company;

/**
 * Сделать класс Figure из задания 09 абстрактным.
 * <p>
 * Определить интерфейс, который позволит перемещать фигуру на заданные координаты.
 * <p>
 * Данный интерфейс должны реализовать только классы Circle и Square.
 * <p>
 * В Main создать массив "перемещаемых" фигур и сдвинуть все их в одну конкретную точку.
 */

public class Main {

    public static void main(String[] args) {

        //создаются фигуры и длины сторон(у круга радиус), центры фигуры по умолчанию 0, 0
        Circle circle = new Circle(4);
        Square square = new Square(20.3);
        Square square1 = new Square(2);

        Figure[] figures = new Figure[3]; // создаем массив на проверку
        figures[0] = square;
        figures[1] = square1;
        figures[2] = circle;

        Shift[] figuresShift = new Shift[3]; //создаем массив перемещаемых фигур
        figuresShift[0] = square;
        figuresShift[1] = square1;
        figuresShift[2] = circle;

        for (int i = 0; i < figures.length; i++) {
            System.out.println("Координаты центра фигуры " + (i + 1) + " X: " + figures[i].getCentreX() + ", Y: " + figures[i].getCentreY());
        }

        for (int i = 0; i < figuresShift.length; i++) { //смещаем фигуры на нужные координаты
            figuresShift[i].shiftX(10);
            figuresShift[i].shiftY(5);
            System.out.println("Координаты центра фигуры " + (i + 1) + " X: " + figures[i].getCentreX() + ", Y: " + figures[i].getCentreY());
        }

    }
}
